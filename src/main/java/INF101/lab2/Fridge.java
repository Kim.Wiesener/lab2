package INF101.lab2;


import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {
    List<FridgeItem> items = new ArrayList<>();
    int max_size = 20;

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()){
            items.add(item);

            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (items.size() >= 1) {
            items.remove(item);
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {

        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem i:items){
            if(i.hasExpired()){
                expiredFood.add(i);
            }
        }
        items.removeAll(expiredFood);
        return expiredFood;
    }
}